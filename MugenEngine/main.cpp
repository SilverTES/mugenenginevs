//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#define SHOW_LOG

#include "Game.h"

int main()
{
    Game game;
    game.run();

	system("pause");
    return log("--- End of main ---\n");
}
