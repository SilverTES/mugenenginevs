//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#include "Game.h"

//------------------------------------------------------------------------------ Init Game
int Game::init()
{


    _config = _data.loadJsonFile("data/config.json");

    std::string appName = _config.at("name");

    _name = appName.c_str();
    _isFullScreen = _config.at("fullscreen");
    _isVsync = _config.at("vsync");
    _isSmooth = _config.at("smooth");
    _screenW = _config.at("width");
    _screenH =  _config.at("height");
    _scaleWin = _config.at("scaleWin"); // scale in windowMode !
    _scaleFull = _config.at("scaleFull"); // 0 = Max zoom !
    _keyFull = false;

    _eventQueue = al_create_event_queue();

    _window->init(_name, _screenW, _screenH, _scaleWin, _scaleFull, _isFullScreen, _isVsync, _isSmooth);

    _framerate.setFramerate(_eventQueue, 60);

    al_hide_mouse_cursor(_window->display());
    //al_show_mouse_cursor(_window->display());
	sf::Joystick::update();
    //_window->showVideoAdapters();

    _manPlayer.add(new Player("GamePad"));
    _manPlayer.add(new Player("Keyboard"));
    //_manPlayer.add(new Player("Zero"));

    //_manPlayer.getPlayer(0)->getController()->showController();

    int cX = _window->centerX();
    int cY = _window->centerY();

    _vecEntityPlayer.emplace_back(EntityPlayer(0, "GamePad", cX, cY));
    _vecEntityPlayer.emplace_back(EntityPlayer(1, "Keyboard", cX, cY));
    //_vecEntityPlayer.emplace_back(EntityPlayer(2, "Zero", cX, cY));

    _manPlayer.getPlayer(0)->getController()->addButton(PAD_UP,    0, 0, 2, 7, 1, -1);
    _manPlayer.getPlayer(0)->getController()->addButton(PAD_DOWN,  0, 0, 2, 7, -1, -1);
    _manPlayer.getPlayer(0)->getController()->addButton(PAD_LEFT,  0, 0, 2, 6, -1, -1);
    _manPlayer.getPlayer(0)->getController()->addButton(PAD_RIGHT, 0, 0, 2, 6, 1, -1);

    _turbo = false;
    _redraw = false;

    ALLEGRO_COLOR colKey = al_map_rgb(250,250,0);
    ALLEGRO_COLOR colJoy = al_map_rgb(0,250,250);

    int graphSize(240);

    _vecGraph.emplace_back(Graph("GamePad X",10,250,graphSize,20,2,colJoy));
    _vecGraph.emplace_back(Graph("GamePad Y",10,280,graphSize,20,2,colJoy));

    _vecGraph.emplace_back(Graph("Keyboard X",10,310,graphSize,20,2,colKey));
    _vecGraph.emplace_back(Graph("Keyboard Y",10,340,graphSize,20,2,colKey));

    _vecGraph.emplace_back(Graph("G UP",   10,10,graphSize,20,1,colJoy));
    _vecGraph.emplace_back(Graph("K UP",   10,40,graphSize,20,1,colKey));

    _vecGraph.emplace_back(Graph("G DOWN", 10,70,graphSize,20,1,colJoy));
    _vecGraph.emplace_back(Graph("K DOWN", 10,100,graphSize,20,1,colKey));

    _vecGraph.emplace_back(Graph("G LEFT", 10,130,graphSize,20,1,colJoy));
    _vecGraph.emplace_back(Graph("K LEFT", 10,160,graphSize,20,1,colKey));

    _vecGraph.emplace_back(Graph("G RIGHT",10,190,graphSize,20,1,colJoy));
    _vecGraph.emplace_back(Graph("K RIGHT",10,220,graphSize,20,1,colKey));



    _font = al_load_font("data/Kyrou.ttf", 8, 0);
    _mainFont = al_load_font("data/Kyrou.ttf", 8, 0);
    _mouseCursor = al_load_bitmap("data/mouse_cursor.png");



    al_set_physfs_file_interface();

    if (!PHYSFS_addToSearchPath("data/myData.zip",0))
        return log("- PHYSFS addToSearchPath error !\n",1);

    if (!PHYSFS_addToSearchPath("data/zero.zip",0))
        return log("- PHYSFS addToSearchPath error !\n",1);


    // Show file in zip archives !
    char **rc = PHYSFS_enumerateFiles("");
    char **i;
    for (i = rc; *i != NULL; i++)
        printf("---> [%s].\n", *i);

    PHYSFS_freeList(rc);

    if ( NULL == (_gamepadSNES = al_load_bitmap("gamepad_snes.png")) )
        return log("- load gamepadSNES bitmap error !\n",1);

    if ( NULL == (_background = al_load_bitmap("bg0.png")) )
        return log("- load background bitmap error !\n",1);



    return log("- init Game OK !\n");
}
//------------------------------------------------------------------------------ Done Game
int Game::done()
{
	_vecEntityPlayer.clear();
    _vecGraph.clear();

    al_destroy_font(_font);
    al_destroy_event_queue(_eventQueue);
    al_destroy_font(_mainFont);
    al_destroy_bitmap(_mouseCursor);
    al_destroy_bitmap(_gamepadSNES);
    al_destroy_bitmap(_background);

    return log("- done Game OK !\n");
}
//------------------------------------------------------------------------------ Update Game
void Game::update()
{

    al_get_mouse_state(&_mouseState);
    _window->getMouse(&_mouseState, _xMouse, _yMouse);

    al_wait_for_event(_eventQueue, &_event);

    if (_input.getKey(ALLEGRO_KEY_ESCAPE))
        _quit = true;

    if (!_input.getKey(ALLEGRO_KEY_SPACE)) _keyFull = false;
    if (_input.getKey(ALLEGRO_KEY_SPACE) && !_keyFull)
    {
        _keyFull = true;
        _window->toggleFullScreen(-1);
        //_window->toggleFullScreen(0);
    }

    if (!_input.getKey(ALLEGRO_KEY_TAB)) _keySwitch = false;
    if (_input.getKey(ALLEGRO_KEY_TAB) && !_keySwitch)
    {
        _keySwitch = true;
        _window->switchMonitor(-1);
    }

    if (_input.getKey(ALLEGRO_KEY_PAD_0)) // scale 0 = Max Scale
        _window->setScale(0);

    if (_input.getKey(ALLEGRO_KEY_PAD_1)) // scale 1
        _window->setScale(1);

    if (_input.getKey(ALLEGRO_KEY_PAD_2)) // scale 2
        _window->setScale(2);

    if (_input.getKey(ALLEGRO_KEY_PAD_3)) // scale 3
        _window->setScale(3);

    if (_input.getKey(ALLEGRO_KEY_PAD_4)) // scale 4
        _window->setScale(4);

    if (_input.getKey(ALLEGRO_KEY_INSERT)) //
        _turbo = true;

    if (_input.getKey(ALLEGRO_KEY_DELETE)) //
        _turbo = false;

    // Test assign button !
    if (_mouseState.buttons & 2)
    {
        log ("*-------------------------*\n");
        log ("*--- ASSIGN CONTROLLER ---*\n");
        log ("*-------------------------*\n");

		Controller::mapButton(_manPlayer.getPlayer(0), PAD_UP);

        //for (unsigned i(0); i<_vecEntityPlayer.size(); i++)
        //{
        //    assignButton(_manPlayer, i, PAD_UP, std::bind(&Game::runAssignButton, this, i, PAD_UP)); // this est l'instance Game:: pour acceder à la methode runAssignButton
        //    assignButton(_manPlayer, i, PAD_DOWN, std::bind(&Game::runAssignButton, this, i, PAD_DOWN));
        //    assignButton(_manPlayer, i, PAD_LEFT, std::bind(&Game::runAssignButton, this, i, PAD_LEFT));
        //    assignButton(_manPlayer, i, PAD_RIGHT, std::bind(&Game::runAssignButton, this, i, PAD_RIGHT));
        //}
    }

    if (_mouseState.buttons & 1)
    {
        _manPlayer.getPlayer(0)->getController()->showController();
    }

	_manPlayer.getPlayer(0)->getController()->pollButton();

    // simulate button pressed !
    if (_input.getKey(ALLEGRO_KEY_Z)) _manPlayer.getPlayer(0)->getController()->setButton(PAD_UP, true);
    if (_input.getKey(ALLEGRO_KEY_S)) _manPlayer.getPlayer(0)->getController()->setButton(PAD_DOWN, true);
    if (_input.getKey(ALLEGRO_KEY_Q)) _manPlayer.getPlayer(0)->getController()->setButton(PAD_LEFT, true);
    if (_input.getKey(ALLEGRO_KEY_D)) _manPlayer.getPlayer(0)->getController()->setButton(PAD_RIGHT, true);

    if (_input.getKey(ALLEGRO_KEY_UP)) _manPlayer.getPlayer(1)->getController()->setButton(PAD_UP, true);
    if (_input.getKey(ALLEGRO_KEY_DOWN)) _manPlayer.getPlayer(1)->getController()->setButton(PAD_DOWN, true);
    if (_input.getKey(ALLEGRO_KEY_LEFT)) _manPlayer.getPlayer(1)->getController()->setButton(PAD_LEFT, true);
    if (_input.getKey(ALLEGRO_KEY_RIGHT)) _manPlayer.getPlayer(1)->getController()->setButton(PAD_RIGHT, true);


    _input.pollJoystick();

    for (unsigned i(0); i<_vecEntityPlayer.size(); i++)
    {
        _vecEntityPlayer[i].update();
    }

    //_deltaTime = (al_get_time() - _prevTime);
    //_prevTime = al_get_time();
    // float speed(100.0 * _deltaTime);

    float speed(1.0);

    for (unsigned i(0); i<_vecEntityPlayer.size(); i++)
    {
        _manPlayer.getPlayer(i)->getController()->pollJoystick();

        if (_manPlayer.getPlayer(i)->getController()->getButton(PAD_UP))
        {
            if (_vecEntityPlayer[i]._y>0)
                _vecEntityPlayer[i]._y -= speed;

            _vecEntityPlayer[i]._up = true;

            if (i<2) _vecGraph[4+i].addValue(100, 80);
        }
        else
        {
            if (i<2) _vecGraph[4+i].addValue(100, 20);
        }

        if (_manPlayer.getPlayer(i)->getController()->getButton(PAD_DOWN))
        {
            if (_vecEntityPlayer[i]._y<_window->screenH())
                _vecEntityPlayer[i]._y += speed;
            _vecEntityPlayer[i]._down = true;

            if (i<2) _vecGraph[6+i].addValue(100, 80);
        }
        else
        {
            if (i<2) _vecGraph[6+i].addValue(100, 20);
        }

        if (_manPlayer.getPlayer(i)->getController()->getButton(PAD_LEFT))
        {
            if (_vecEntityPlayer[i]._x>0)
                _vecEntityPlayer[i]._x -= speed;
            _vecEntityPlayer[i]._left = true;

            if (i<2) _vecGraph[8+i].addValue(100, 80);
        }
        else
        {
            if (i<2) _vecGraph[8+i].addValue(100, 20);
        }

        if (_manPlayer.getPlayer(i)->getController()->getButton(PAD_RIGHT))
        {
            if (_vecEntityPlayer[i]._x<_window->screenW())
                _vecEntityPlayer[i]._x += speed;
            _vecEntityPlayer[i]._right = true;

            if (i<2) _vecGraph[10+i].addValue(100, 80);
        }
        else
        {
            if (i<2) _vecGraph[10+i].addValue(100, 20);
        }
    }

    _framerate.pollFramerate();


//    for (unsigned i(0); i<_vecGraph.size(); i++)
//    {
//        float value(random(0,80));
//    }

    _vecGraph[0].addValue(_screenW, _vecEntityPlayer[0]._x);
    _vecGraph[1].addValue(_screenH, _vecEntityPlayer[0]._y);

    _vecGraph[2].addValue(_screenW, _vecEntityPlayer[1]._x);
    _vecGraph[3].addValue(_screenH, _vecEntityPlayer[1]._y);

    al_flush_event_queue(_eventQueue);
}
//------------------------------------------------------------------------------ Render Game
void Game::render()
{
    _window->beginRender();
    //al_hold_bitmap_drawing(true);

    al_clear_to_color(al_map_rgb(25,40,50));

    al_draw_bitmap(_background,0,0,0);

    drawGrid(_config.at("gridW"),_config.at("gridH"),al_map_rgba(30,40,50,50), _screenW, _screenH);

    al_draw_bitmap(_gamepadSNES,
                   _window->centerX()-al_get_bitmap_width(_gamepadSNES)/2+140,
                   _window->centerY()-al_get_bitmap_height(_gamepadSNES)/2,
                   0);

    for (unsigned i(0); i<_vecGraph.size(); i++)
    {
        _vecGraph[i].render(_window);
    }


//    float prevX( _vecEntityPlayer[0]._x);
//    float prevY( _vecEntityPlayer[0]._y);
//    for (unsigned i(0); i<_vecEntityPlayer.size(); i++)
//    {
//        al_draw_line(prevX,prevY, _vecEntityPlayer[i]._x, _vecEntityPlayer[i]._y, al_map_rgba(250,10,10,25),0);
//        prevX = _vecEntityPlayer[i]._x;
//        prevY = _vecEntityPlayer[i]._y;
//    }
//    al_draw_line(prevX,prevY, _vecEntityPlayer[0]._x, _vecEntityPlayer[0]._y, al_map_rgba(250,10,100,25),0);

    for (unsigned i(0); i<_vecEntityPlayer.size(); i++)
    {
        _vecEntityPlayer[i].render(_font);
    }

	if (_manPlayer.getPlayer(0)->getController()->isAssignButton())
	{
		al_draw_textf(_font, al_map_rgb(200, 100, 80), _window->centerX(), _window->centerY() - 20, -1,
					  "PLAYER : %s", (_manPlayer.getPlayer(0)->getController()->mapPlayer()->getName()).c_str());

		al_draw_textf(_font, al_map_rgb(200, 100, 80), _window->centerX(), _window->centerY(), -1,
					  "ASSIGN BUTTON : %s", (SNESButtonDico[_manPlayer.getPlayer(0)->getController()->mapIdButton()]).c_str());
	}

	if (_manPlayer.getPlayer(0)->getController()->isAssignButtonOK())
		al_draw_filled_circle(200, 200, 20, al_map_rgb(255, 255, 0));

    al_draw_textf(_mainFont, al_map_rgb(250,250,200), 180, 12, 0,
                  "_isFullScreen : %i", _window->isFullScreen());

    al_draw_textf(_mainFont, al_map_rgb(25,250,200), 180, 22, 0,
                  "window Pos : %i , %i", _window->x(), _window->y());

    al_draw_textf(_mainFont, al_map_rgb(205,250,200), 2, 2, 0,
                  "currentMonitor : %i", _window->currentMonitor(_window->display()));

    al_draw_textf(_mainFont, al_map_rgb(205,250,200), 2, 12, 0,
                  "currentMonitor selected : %i", _window->currentMonitor());

    al_draw_textf(_mainFont, al_map_rgb(205,250,200), 2, 32, 0,
                  "currentMonitor Pos : %i , %i", _window->currentMonitorX(), _window->currentMonitorY());

    al_draw_textf(_mainFont, al_map_rgb(205,250,200), 2, 42, 0,
                  "currentMonitor Size : %i , %i", _window->currentMonitorW(), _window->currentMonitorH());

    al_draw_textf(_mainFont, al_map_rgb(205,20,200), 2, 52, 0,
                  "view Pos : %i , %i", _window->viewX(), _window->viewY());

    al_draw_textf(_mainFont, al_map_rgb(205,20,200), 2, 62, 0,
                  "view Size : %i , %i", _window->viewW(), _window->viewH());

    al_draw_textf(_mainFont, al_map_rgb(205,20,20), 140, 52, 0,
                  "_scaleWin : %i", _window->scaleWin());

    al_draw_textf(_mainFont, al_map_rgb(205,20,20), 140, 62, 0,
                  "_scaleFull: %i", _window->scaleFull());


//    al_draw_textf(_mainFont, al_map_rgb(205,200,200), 320, 20, 0,
//                  "debug: %f", pourcent(_screenW, _vecEntityPlayer[0]._x ));
//
//    al_draw_textf(_mainFont, al_map_rgb(205,200,20), 4, _screenH-16, 0,
//                  "al_get_time: %f", al_get_time());

    al_draw_line(0,_yMouse+.5,
                 _screenW,_yMouse+.5,
                 al_map_rgba(55,125,100,25),0);

    al_draw_line(_xMouse+.5,0,
                 _xMouse+.5,_screenH,
                 al_map_rgba(55,125,100,25),0);
    al_draw_bitmap(_mouseCursor, _xMouse, _yMouse, 0);


    al_draw_textf(_mainFont, al_map_rgb(205,200,200), _screenW -64, 2, 0,
                  "FPS: %i", _framerate.getFramerate());

    //al_hold_bitmap_drawing(false);
    _window->endRender();


}
//------------------------------------------------------------------------------ Test debug
void Game::runAssignButton(int player, int id)
{
    _window->beginRender();
    al_clear_to_color(al_map_rgb(0,20,40));

    al_draw_textf(_font, al_map_rgb(200,100,80), _window->centerX(), _window->centerY()-20, -1,
                  "PLAYER : %s", (_manPlayer.getPlayer(player)->getName()).c_str());

    al_draw_textf(_font, al_map_rgb(200,100,80), _window->centerX(), _window->centerY(), -1,
                  "ASSIGN BUTTON : %s", (SNESButtonDico[id]).c_str());
    _window->endRender();

}

