//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------

#include "Data.h"

Data::Data()
{
    //ctor
}

Data::~Data()
{
    //dtor
}

nlohmann::json Data::loadJsonFile(std::string const filename)
{
    std::string jdata("");

    std::fstream jsonFile(filename);
    if (!jsonFile.is_open())
    {

        return log("JSON file not found ! \n");
    }
    else
    {
        std::string line("");
        while (!jsonFile.eof())
        {
            getline( jsonFile, line); // lecture d'une ligne de fichier
            jdata += line;
        }
    }
    jsonFile.close();
    log("Json File Loaded : "); log(filename); log("\n");
    return nlohmann::json::parse(jdata);
}

void Data::saveJsonFile(std::string const filename, nlohmann::json data)
{
    std::ofstream jsonFile(filename);
    if (jsonFile.is_open())
    {
        jsonFile << data.dump(4); // permet de cr�er les indentations �l�ments , sinon il serait align� sur une seule m�me ligne !
    }
    else
    {
        log("JSON file save impossible ! \n");
    }
    jsonFile.close();
    log("Json File Saved : "); log(filename); log("\n");
}

// In Lua 5.0 reference manual is a table traversal example at page 29.
void Data::PrintTable(lua_State *L)
{
    lua_pushnil(L);

    while(lua_next(L, -2) != 0)
    {
        if(lua_isstring(L, -1))
            //printf("%s = %s\n", lua_tostring(L, -2), lua_tostring(L, -1));
            std:: cout << lua_tostring(L, -2) << " = " << lua_tostring(L, -1) << "\n";
        else if(lua_isnumber(L, -1))
            //printf("%s = %d\n", lua_tostring(L, -2), lua_tonumber(L, -1));
            std:: cout << lua_tostring(L, -2) << " = " << lua_tostring(L, -1) << "\n";
        else if(lua_istable(L, -1))
        {
            PrintTable(L);
        }
        lua_pop(L, 1);
    }
}
