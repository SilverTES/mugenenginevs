//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------

#include "MugenEngine.h"

MugenEngine::MugenEngine()
{
    _quit = false;
    log("- MugenEgine Created !\n");
}
MugenEngine::~MugenEngine()
{
    log("- MugenEgine Deleted !\n");
}

int MugenEngine::initMugenEngine()
{

    // Initialiaze Allegro5
    if(!al_init())
        return log("failed to initialize allegro!\n",1);

    // --- Install All devices
    if (!al_install_audio())
        return log("- Unable to install Audio ! \n",1);
    if (!al_install_keyboard())
        return log("- Unable to install Keyboard ! \n",1);
    if (!al_install_mouse())
        return log("- Unable to install Mouse ! \n",1);

//    if (!al_install_joystick())
//        return log(1,"- Unable to install Joystick ! \n");

    // --- Init All addons
    if (!al_init_primitives_addon())
        return log("- Unable to init Primitives Addon ! \n",1);
    if (!al_init_image_addon())
        return log("- Unable to init Image Addon ! \n",1);
    if (!al_init_font_addon())
        return log("- Unable to init Font Addon ! \n",1);
    if (!al_init_ttf_addon())
        return log("- Unable to init TTF Addon ! \n",1);
    if (!al_init_acodec_addon())
        return log("- Unable to init ACODEC Addon ! \n",1);


    if (!al_reserve_samples(32)) // nombre de son simultannée dans le "Voice" par défaut  [default mixer]
        return log("- Unable to reserve samples",1);


    al_set_new_display_flags(ALLEGRO_OPENGL);
    //al_set_new_display_flags(ALLEGRO_DIRECT3D);
    _window = std::make_shared<Window>();

#ifdef SHOW_LOG
    std::cout << "--- PHYSFS version : "<< PHYSFS_VER_MAJOR << "." << PHYSFS_VER_MINOR << "." << PHYSFS_VER_PATCH << "\n";
#endif // SHOW_LOG
    if (!PHYSFS_init(NULL))
        return log("- PHYSFS init error !\n",1);


    if(init())
        return log("- init Game error !\n",1);

    return log("- init MugenEngine OK !\n");
}
int MugenEngine::doneMugenEngine()
{
    if (!PHYSFS_deinit())
        return log("- PHYSFS deinit error !\n",1);

    _window->done();

    if(done())
        return log("- done Game error !\n",1);

    return log("- done MugenEngine OK !\n");
}

int MugenEngine::loopMugenEngine()
{
    while (!_quit)
    {
        update();
        render();
    }

    return log("- loop MugenEngine OK !\n");
}


int MugenEngine::run()
{
    if (initMugenEngine())
        return log("- init MugenEngine error !\n",1);

    if (loopMugenEngine())
        return log("- loop MugenEngine error !\n",2);

    if (doneMugenEngine())
        return log("- done MugenEngine error !\n",3);

    return log("- run MugenEngine OK !\n");
}




