//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#include "Player.h"

Player::Player(std::string name):
_name(name)
{
   log("+++ Player "+_name+" \n");
    _life = 100;
    _force = 32;
    _power = 8;
    _controller = new Controller();
}
Player::~Player()
{
    log("--- Player \n");
    delete _controller;
}
Player * Player::getData()
{
    return this;
}
Controller * Player::getController()
{
    return _controller;
}
std::string Player::getName()
{
    return _name;
}


PlayerManager::PlayerManager()
{
    log("+++ PlayerManager  \n");
}

PlayerManager::~PlayerManager()
{
    log("--- PlayerManager \n");
    for (auto & it: _vecPlayer)
    {
        delete it;
    }
    _vecPlayer.clear();
}

void PlayerManager::add(Player * player)
{
    _vecPlayer.emplace_back(player);
}

int PlayerManager::getNum()
{
    return _vecPlayer.size();
}

Player * PlayerManager::getPlayer(unsigned index)
{
    if (index < _vecPlayer.size())
    {
        return _vecPlayer[index]->getData();
    }
    return NULL;
}
