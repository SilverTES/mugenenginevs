//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#include "Misc.h"

int random(int beginRange, int endRange)
{
    return (rand() % endRange) + beginRange;
}
float pourcent(float maxValue, float value)
{
    return (100*value)/maxValue;
}
float proportion(float maxValue, float value, float range)
{
    return (range*value)/maxValue;
}

//------------------------------------------------------------------------------ Debug
void showVideoAdapters()
{
    for (int i(0); i<al_get_num_video_adapters(); i++)
    {
        ALLEGRO_MONITOR_INFO info;
        al_get_monitor_info(i, &info);

        std::cout << "- Adapter : "<< i << "\n";
        std::cout << "- pos X   : "<< info.x1 << "\n";
        std::cout << "- pos Y   : "<< info.y1 << "\n";
        std::cout << "- size W  : "<< info.x2 << "\n";
        std::cout << "- size H  : "<< info.y2 << "\n";

    }
}
void drawGrid(int sizeX, int sizeY, ALLEGRO_COLOR color, int screenW, int screenH)
{

    for (int i(0); i<(screenW/sizeX); i++)
    {
        al_draw_line(i*sizeX+.5, .5, i*sizeX+.5, screenH+.5, color,0);
    }

    for (int i(0); i<(screenH/sizeY)+1; i++)
    {
        al_draw_line(.5, i*sizeY+.5, screenW+.5, i*sizeY+.5, color,0);
    }
}
